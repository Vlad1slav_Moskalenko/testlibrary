<head>
    <meta charset="UTF-8">
    <title>Library</title>
</head>
<body>
<?
    include_once 'DB.php';

    //$start = microtime(true);

    $db = new DB();
    $books = $db->getBooks();

    //echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.';

    echo "Список книг у которых 3 и более соавторов:</br></br>";
    foreach ($books as $value) {
        if ($value)
            echo '- '.$value.'</br>';
    }
?>
</body>
</html>
