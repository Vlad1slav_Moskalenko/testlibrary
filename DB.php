<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 23.10.2017
 * Time: 13:42
 */

/*
 * В базе данных две таблицы: `books`(содержит три колонки: id книги, название книги и id авторов записанные через '#')
 * и `authors` (содержит две колонки: id автора и имя автора). Запрос возвращает названия книг, у которых в поле
 * authors_id символов '#' больше или равно 3.
 */

class DB
{
    private $connection;

    private function getConnection()
    {
        $host = 'localhost';
        $nameDB = 'library';
        $username = 'root';
        $password = '';
        $charset = 'utf8';

        $dsn = "mysql:host=$host;dbname=$nameDB;charset=$charset";

        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        try {
            $this->connection = new PDO($dsn, $username, $password, $opt);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function getBooks ()
    {
        if (!$this->connection)
            $this->getConnection();

        $query = "SELECT b.book_title, count(*) as cnt FROM books as b JOIN authors_books as ab ON b.id = ab.book_id GROUP BY b.id HAVING cnt >= 3";

        $result = array();
        foreach (($this->connection->query($query)) as $row) {
            $result[] = array_values($row)[0];
        }

        return $result;
    }

    public function __construct()
    {
        $this->getConnection();
    }

}